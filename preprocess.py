import warnings
warnings.filterwarnings('ignore')

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

from imblearn.over_sampling import SMOTE
from collections import Counter


#Preprocess Data#
def preprocess(df):
    """Preprocess clean the csv dataset.

    Args:
        df: Pandas dataframe of the raw csv dataset.
    Returns:
        preprocessed and cleaned dataframed.
    """

    df = df.drop(["Date","Location"],1)     #Drop redndant colums from data
    df = df.dropna()      #Drop all the rows containing NaN values
    #List of the columns having categorical values
    category_list = ['WindGustDir', 'WindDir9am', 'WindDir3pm', 'RainToday', 
                    'RainTomorrow']
    #Encoding the categorical values to numerical value with LabelEncoder
    encoder = LabelEncoder()
    for category in category_list:
        df[category] = encoder.fit_transform(df[category])
    #Preprocessed Data
    return df

def plot_label(df, y):
    #plot the dataset by lable category
    plt.figure(figsize=(8, 8))
    sns.countplot('RainTomorrow', data=df)
    plt.title('Baised Labels')
    plt.savefig('plots/before_oversampling.png')
    # plt.show()

    sns.countplot(y)
    plt.title('Unbaised Labels')
    plt.savefig('plots/after_oversampling.png')
    # plt.show()


def oversampling(feature, label):
    """Oversample data with baised label with SMOTE.

    Args:
        feature: features of data to be trained.
        label: label/output value of data
    Returns:
        oversampled unbaised data.
    """
    counter = Counter(label)
    # print('Before Oversampling:',counter)

    # transform the dataset
    oversample = SMOTE()
    feature, label = oversample.fit_resample(feature, label)
    # summarize the new class distribution
    counter = Counter(label)
    # print('After Oversampling:',counter)
    return feature, label

def scale_features(feature):
    """Scale the features to get uniform values .

    Args:
        feature: features of data to be trained.
    Returns:
        features with scaled and uniform values.
    """ 
    scaler = MinMaxScaler()
    scaled_data = scaler.fit_transform(feature)
    return scaled_data

def prepare_training(df):
    """Apply all preprocessing methods and split
       the preprocessed data into training and testing .

    Args:
        df: pandas dataframe of the csv dataset. .
    Returns:
        training and testing features and label data.
    """ 
    processed_data = preprocess(df)
    features = processed_data.drop("RainTomorrow",1)   #Feature Matrix
    y = processed_data["RainTomorrow"]     #Target Variable
    features,y = oversampling(features, y)  #Oversampling Unbalanced Dataset 
    plot_label(processed_data, y)     
    X = scale_features(features)         #Scale features
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20,random_state=42)
    return X, y, X_train, X_test, y_train, y_test


if __name__ == '__main__':
    #Import csv Dataset as Dataframe
    df = pd.read_csv('weatherAUS.csv')
    #Dataset Info
    print(df.head())
    print("Data Description:\n",df.describe())
    X, y, X_train, X_test, y_train, y_test = prepare_training(df)
    print("Preprocessing Finished")


