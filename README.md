# Predict rain tomorrow

### The above code is the solution of Kaggle task for predicting rain next day given the weather condition of today. The dataset used is australian weather dataset *weatherAus.csv*

### The dataset is trained with Support Vector Machine with polynomial kernel. It is trained on 80% of data and tested on 20% of data.

### The dataset for predicting rain tomorrow contains different features to determine whether it will rain tomorrow or not Since the final output depends on various continuous features. The classes in the dataset are not linearly separable. This means there is not a very great correlation between the features to determine the output. In the simple words by looking on the dataset we cannot figure out how the output changes distinctly with the change in the input features. So the Support Vector Machine algorithm will give the best results on training the dataset since logistic regression and Decision Tree seems to struggle with the randomness of the features.

### Particularly the SVM with polynomial kernel will be the best for this problem. Because there are many overlapping data points in the dataset. Due to the overlapping of the dataset predicting the decision boundary is very hard for linear SVM. So with the help of polynomial SVM the data points are mapped in the higher dimension and relationships between the observations are computed. That will make our support vector classifier to compute the relationship between the features more precisely.

### Also trained the dataset with logistic regression but the cross validation accuracy was quite low (86%) compared with that of SVM (98%)


## This consists of files:
    
* preprocess.py
* training.py
* test.py

## Which all can be run individually.

### To train the dataset using SVM just run *training.py*.
### To test the saved trained model just jun *test.py*. 

#### Output of *preprocess.py*.
![alternativetext](outputs/preprocess-output.png)

#### Output of *training.py*.
![alternativetext](outputs/training-output.png)

#### Output of *test.py*.
![alternativetext](outputs/predict-output.png)