from preprocess import prepare_training

import pickle
import pandas as pd
import numpy as np


from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score



def svm_cross_validation(X, y):
    """Use cross validation for checking the performance of SVM model.

    Args:
        X: features/values of preprocessed data.
        y: label/output value of  preprocessed data
    Returns:
        None.
    """
    clf = SVC(kernel='poly', C=1) #Support Vector Classification class.
    scores = cross_val_score(clf, X, y, cv=10)  #Score for cross validation 
    accuracy = np.mean(scores) #Accuracy of cross validation
    print("Cross Validation Accuracy(SVM):",accuracy)


def train_svm(X, y, filename):
    """Train the training set(80%) of whole dataset with SVM.

    Args:
        X: features/values of processed training  data.
        y: label/output value of  preprocessed training data
        filename: name of the file to save trained model
    Returns:
        trained SVM model.
    """
    model = SVC(kernel='poly', C=1)
    model.fit(X, y)
    model_file = 'model/' + filename
    # save the model
    pickle.dump(model, open(model_file, 'wb'))
    return model


def log_reg_cross_validation(X, y):
    """Use cross validation for checking the performance of Logistic Regression model
    and save model in the pickle file.

    Args:
        X: features/values of preprocessed data.
        y: label/output value of  preprocessed data
    Returns:
        None.
    """
    model = LogisticRegression() #Logistic Regression Classification class.
    scores = cross_val_score(model, x, y, cv=10)  #Score for cross validation
    accuracy = np.mean(scores)  #Accuracy of cross validation
    print("Cross Validation Accuracy(Logistic Regression):",accuracy)


# def train_log_reg(x, y, filename):
#     """Train the training set(80%) of whole dataset with Logistic Regression
#     and save model in the pickle file.

#     Args:
#         X: features/values of processed training  data.
#         y: label/output value of  preprocessed training data
#         filename: name of the file to save trained model
#     Returns:
#         trained Logistic Regression model.
#     """
#     model = LogisticRegression()
#     model.fit(x,y)
#     model_file = 'model/' + filename
#     # save the model to disk
#     pickle.dump(model, open(model_file, 'wb'))
#     return model


#Driver Function
if __name__ == '__main__':
    #Import csv Dataset as Dataframe
    df = pd.read_csv('weatherAUS.csv')
    X, y, X_train, X_test, y_train, y_test = prepare_training(df)
    svm_cross_validation(X, y)
    log_reg_cross_validation(X, y)
    print("Training Set is being trained with SVM...........")
    train_svm(X_train, y_train, 'trained_svm.sav')
    print("Training SVM finished!!!")
    


