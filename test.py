from preprocess import prepare_training

import pandas as pd
import pickle

from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report, confusion_matrix
 

def predict_model(test_feature, test_label, filename):
    """Predict the saved SVM model with test data.

    Args:
        test_feature:  test features/values of preprocessed data.
        test_label:  test label/output value of  preprocessed data
        filename: name of saved model
    Returns:
        Prediction value.
    """
    model_file = 'model/' + filename
    # load the model
    model = pickle.load(open(model_file, 'rb'))
    pred = model.predict((test_feature)) #Predicted Values
    model_accuracy = accuracy_score(test_label, pred) #Model Accuracy
    print("Model Accuracy:",model_accuracy)
    return pred

def show_result(test_feature, test_label):
    """Show the result of saved SVM model.

    Args:
        test_feature:  test features/values of preprocessed data.
        test_label:  test label/output value of  preprocessed data
    Returns:
        None.
    """
    pred = predict_model(test_feature, test_label, 'trained_svm.sav')
    print('\n********Confusion Matrix********')
    print(confusion_matrix(test_label,pred))
    print(classification_report(y_test,pred))


#Driver Function
if __name__ == '__main__':
    df = pd.read_csv('weatherAUS.csv')
    X, y, X_train, X_test, y_train, y_test = prepare_training(df)
    show_result(X_test, y_test)


